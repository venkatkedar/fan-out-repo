package com.projectx.fanoutcontroller.bootstrap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.projectx","com.common"})
public class FanOutApplication {

	public static void main(String[] args) {
		SpringApplication.run(FanOutApplication.class, args);
	}

}
