package com.projectx.fanoutcontroller.controllers;

import com.projectx.fanoutservice.model.FanOutVO;

import java.util.Optional;

import com.projectx.fanoutservice.api.IFanOutService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;


//@RestController
//@RequestMapping("/v1")
@Slf4j
public class FanOutController {
    private IFanOutService fanoutService;

    @Autowired
    public FanOutController(IFanOutService fanoutService){
        this.fanoutService=fanoutService;
    } 

    @RequestMapping(value="/fan-out", method=RequestMethod.POST)
    public Mono<FanOutVO> addFanOut(@RequestBody FanOutVO fanout) {
        log.info("fanout : "+fanout);
        return fanoutService.addFanOut(fanout);
    }
    

    @RequestMapping(value="/fan-out/{id}", method=RequestMethod.PUT)
    public Mono<FanOutVO> updateFanOut(@PathVariable("id") String id, @RequestBody FanOutVO fanout) {
        log.info("fanout : "+fanout);
        fanout.setId(id);
        return fanoutService.updateFanOut(fanout);
    }

    @RequestMapping(value="/fan-out/{id}", method=RequestMethod.GET)
    public Mono<FanOutVO> getFanOut(@PathVariable("id") String id) {
        log.info("get fanout with id: "+id);
        return fanoutService.getFanOut(id);
    }

    @RequestMapping(value="/fan-out", method=RequestMethod.DELETE)
    public Mono<Void> deleteFanOut(@RequestBody FanOutVO fanout) {
        log.info("fanout : "+fanout);
        return fanoutService.deleteFanOut(fanout);
    }

    @RequestMapping(value="/fan-out/{id}", method=RequestMethod.DELETE)
    public Mono<Void> deleteFanOutById(@PathVariable("id") String id) {
        log.info("deleting fanout with id : "+id);
        return fanoutService.deleteFanOutById(id);
    }
}
