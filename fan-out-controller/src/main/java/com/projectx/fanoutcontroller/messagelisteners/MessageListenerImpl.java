package com.projectx.fanoutcontroller.messagelisteners;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.projectx.fanoutservice.model.ContentVO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.web.client.RestTemplate;

import lombok.extern.slf4j.Slf4j;

@EnableBinding(Sink.class)
@Slf4j
public class MessageListenerImpl {
    ExecutorService executor;

    @Value("num_threads")
    private int numThreads;

    @Autowired
    private RestTemplate restTemplate;

    public MessageListenerImpl(){
        executor=Executors.newFixedThreadPool(numThreads);
    }
    
    @StreamListener("input")
    public void onMessage(ContentVO content ){
        CompletableFuture.supplyAsync(()-> content,executor)
                         .thenApply(c->{
                             //call follow service
                             log.debug("fetching followers from follow service for userId: "+c.getSupplier().getId());

                             return c;
                         })
                         .thenApply(f->{

                             // combine contetnt with the results of follow service
                             return f;
                         })
                         .thenApply(r->{
                             // send results to redis service
                             return r;
                         });

    }

    
}
