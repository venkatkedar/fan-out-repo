package com.projectx.fanout.dao.api;

import com.common.dao.api.ReactiveBaseDao;
import com.projectx.fanout.dao.model.FanOutEO;

import reactor.core.publisher.Mono;

public interface IFanOutDao extends ReactiveBaseDao<FanOutEO>{
    
}
