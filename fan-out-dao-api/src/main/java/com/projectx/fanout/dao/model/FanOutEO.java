package com.projectx.fanout.dao.model;

import com.common.dao.entities.AbstractEO;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document
public class FanOutEO extends AbstractEO {
    
    private String firstname;
    private String lastname;    
    private String email;
    
}
