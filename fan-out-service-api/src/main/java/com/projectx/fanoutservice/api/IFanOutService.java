package com.projectx.fanoutservice.api;


import com.common.service.exceptions.ServiceException;
import com.projectx.fanoutservice.model.FanOutVO;

import reactor.core.publisher.Mono;

public interface IFanOutService {
    Mono<FanOutVO> addFanOut(FanOutVO user) throws ServiceException;
    Mono<FanOutVO> updateFanOut(FanOutVO user) throws ServiceException;
    Mono<Void> deleteFanOut(FanOutVO user) throws ServiceException;
    Mono<Void> deleteFanOutById(String id)throws ServiceException;
    Mono<FanOutVO> getFanOut(String userId) throws ServiceException;
}
