package com.projectx.fanoutservice.model;

import com.common.service.valueobjects.AssetVO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ContentVO extends AssetVO {
    private String text;
    private String contentUrl;    
}
