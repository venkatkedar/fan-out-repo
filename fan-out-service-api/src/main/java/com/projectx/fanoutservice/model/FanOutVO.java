package com.projectx.fanoutservice.model;

import com.common.service.valueobjects.AbstractVO;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@NoArgsConstructor
public class FanOutVO extends AbstractVO{
    //@NonNull
    private String firstname;
    //@NonNull
    private String lastname;
    //@NonNull
    private String email;
}
