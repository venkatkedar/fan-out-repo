package com.projectx.fanoutservice.impl;

import java.util.Date;

import com.common.dao.exceptions.EntityNotFoundException;
import com.common.service.exceptions.NotFoundException;
import com.common.service.exceptions.ServiceException;
import com.common.utils.converter.Converter;
import com.projectx.fanoutservice.api.IFanOutService;
import com.projectx.fanoutservice.model.FanOutVO;
import com.projectx.fanout.dao.api.IFanOutDao;
import com.projectx.fanout.dao.model.FanOutEO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

@NoArgsConstructor
@Slf4j
@Service
public class FanOutServiceImpl implements IFanOutService {

    private IFanOutDao fanoutdao;

    @Autowired
    public FanOutServiceImpl(IFanOutDao fanoutdao){
        this.fanoutdao=fanoutdao;       
    }

    @Override
    public Mono<FanOutVO> addFanOut(FanOutVO fanout) throws ServiceException{
        log.info("fanout in service :"+fanout);
        Date d=new Date();
        fanout.setLastUpdatedTime(d);
        FanOutEO uEO=Converter.convertUToV(fanout, FanOutEO.class);
        Mono<FanOutEO> fanoutEO= fanoutdao.add(uEO);
        
        return fanoutEO.map(f->Converter.convertUToV(f, FanOutVO.class));
    }

    @Override
    public Mono<FanOutVO> updateFanOut(FanOutVO fanout) throws ServiceException{
        Date d=new Date();
        fanout.setLastUpdatedTime(d);
        FanOutEO uEO=Converter.convertUToV(fanout, FanOutEO.class);
        Mono<FanOutEO> fanoutEO= fanoutdao.update(uEO);
        
        return fanoutEO.map(f->{
            return Converter.convertUToV(f, FanOutVO.class);
        });
    }

    @Override
    public Mono<Void> deleteFanOut(FanOutVO fanout) throws ServiceException{
        FanOutEO uEO=Converter.convertUToV(fanout, FanOutEO.class);
        return fanoutdao.delete(uEO);
    }

    @Override
    public Mono<Void> deleteFanOutById(String id) throws ServiceException{
        return fanoutdao.deleteById(id);
    }

    @Override
    public Mono<FanOutVO> getFanOut(String fanoutId) throws ServiceException{
        Mono<FanOutEO> uEO= fanoutdao.findById(fanoutId)
                                                              .doOnError(error -> {
                                                                  if(error instanceof EntityNotFoundException) 
                                                                  throw new NotFoundException(error.getMessage());
                                                                  else throw new ServiceException(error.getMessage());
                                                              });
        return uEO.map(fanout->Converter.convertUToV(fanout, FanOutVO.class));
    }

   

    
}
