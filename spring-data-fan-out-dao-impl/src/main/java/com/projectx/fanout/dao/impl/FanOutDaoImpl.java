package com.projectx.fanout.dao.impl;

import com.common.dao.spring.impl.AbstractSpringDataReactiveDaoImpl;
import com.common.dao.spring.impl.SpringDataBaseDaoImpl;
import com.projectx.fanout.dao.api.IFanOutDao;
import com.projectx.fanout.dao.model.FanOutEO;
import com.projectx.fanout.dao.repository.FanOutEOReactiveRepository;
import com.projectx.fanout.dao.repository.FanOutEORepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

@Data
@Repository
@Slf4j
public class FanOutDaoImpl extends AbstractSpringDataReactiveDaoImpl<FanOutEO,FanOutEOReactiveRepository> implements IFanOutDao {
    private FanOutEOReactiveRepository repository;
    
    @Autowired
    public FanOutDaoImpl(FanOutEOReactiveRepository repository){
        super(repository);
        this.repository=repository;
    }

    

    

}
