package com.projectx.fanout.dao.repository;

import com.common.dao.spring.api.AbstractEOReactiveRepository;
import com.projectx.fanout.dao.model.FanOutEO;

import org.springframework.stereotype.Repository;

@Repository
public interface FanOutEOReactiveRepository extends AbstractEOReactiveRepository<FanOutEO>{
    
}
