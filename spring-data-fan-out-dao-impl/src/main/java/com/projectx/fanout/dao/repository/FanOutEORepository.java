package com.projectx.fanout.dao.repository;

import com.common.dao.spring.api.AbstractEORepository;
import com.projectx.fanout.dao.model.FanOutEO;

public interface FanOutEORepository extends AbstractEORepository<FanOutEO>{
    
}
